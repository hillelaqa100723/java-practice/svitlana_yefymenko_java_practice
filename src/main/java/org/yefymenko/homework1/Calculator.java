package org.yefymenko.homework1;

public class Calculator {
    public static void main(String[] args) {
        int a = 17;
        int b = 6;
        int rez;

        rez = a + b;
        System.out.println("Sum: " + rez);
        rez = a - b;
        System.out.println("Odd: " + rez);
        rez = a * b;
        System.out.println("Mult: " + rez);
        if (b != 0) {
            rez = a / b;
            System.out.println("Div: " + rez);
            rez = a % b;
            System.out.println("Rem: " + rez);
        } else {
            System.out.println("You cannot divide on 0!");
        }
    }
}
